const mongoose = require('mongoose');
//const config = require('config');
//const db = config.get('mongoURI');

const connectDB = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://Nour:ents8hYc1PdqxAEg@cluster0.p6aw3.mongodb.net/test?retryWrites=true&w=majority',
      {
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
      }
    );
    console.log('Mongo DB connected ...');
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
module.exports = connectDB;
