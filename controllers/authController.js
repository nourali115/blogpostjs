const expres = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { validationResult } = require('express-validator');
const User = require('../models/User');

async function login(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send({ errors: errors.array() });
    }
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email });
        if (!user) {
            return res
                .status(400)
                .send({ errors: [{ msg: 'invalid credentials' }] });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res
                .status(400)
                .send({ errors: [{ msg: 'Invalid credentials' }] });
        }

        const payload = {
            user: user,
        };
        jwt.sign(
            payload,
            config.get("jwtToken"),
            { expiresIn: 360000 },
            (err, token) => {
                if (err) throw err;
                res.json(token);
            }
        );
    } catch (error) {
        console.log(error);
        res.status(500).send('server error');
    }
}

async function getUser(req, res) {
    try {
        const user = await User.findById(req.user._id);
        res.json(user);
    } catch (error) {
        console.error(error);
        res.status(500).send('server error');
    }
}

module.exports = {
    login,
    getUser
}