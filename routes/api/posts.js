const expres = require('express');
const router = expres.Router();
const User = require('../../models/User');
const Post = require('../../models/Post');
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');
const { addPost, getPosts, findPost, deletePost, likePost, unlikePost, addComment } = require('../../controllers/postController');

router.post(
  '/',
  [auth, [check('text', 'text is required').not().isEmpty()]],
  addPost
);

router.get('/', auth, getPosts);

router.get('/:id', auth, findPost);

router.delete('/:id', auth, deletePost);

router.put('/like/:id', auth, likePost);

router.put('/unlike/:id', auth, unlikePost);

router.post(
  '/comment/:id',
  [auth, [check('text', 'text is required').not().isEmpty()]],
  addComment
);

router.delete('/comment/:id/:comment_id', auth,);

module.exports = router;
