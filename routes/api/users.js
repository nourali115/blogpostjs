const expres = require('express');
const router = expres.Router();
const { check } = require('express-validator');
const { register } = require('../../controllers/userController')

router.post(
  '/',
  [
    check('name', 'name is required').not().isEmpty(),
    check('email', 'please, enter a valid Email').isEmail(),
    check('password', 'please enter a valid password').isLength({ min: 6 }),
  ],
  register
);

module.exports = router;
